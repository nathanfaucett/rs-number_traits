use core::f32::{NAN, NEG_INFINITY};

use libc::c_float;

#[link_name = "m"]
extern "C" {
    pub fn acosf(n: c_float) -> c_float;
    pub fn asinf(n: c_float) -> c_float;
    pub fn atan2f(a: c_float, b: c_float) -> c_float;
    pub fn atanf(n: c_float) -> c_float;
    pub fn coshf(n: c_float) -> c_float;
    pub fn sinhf(n: c_float) -> c_float;
    pub fn tanf(n: c_float) -> c_float;
    pub fn tanhf(n: c_float) -> c_float;
    pub fn log1pf(n: c_float) -> c_float;
}

extern "rust-intrinsic" {
    pub fn cosf32(x: f32) -> f32;
    pub fn sinf32(x: f32) -> f32;
    pub fn logf32(x: f32) -> f32;
    pub fn sqrtf32(x: f32) -> f32;
}

pub use self::acosf as acosf32;
pub use self::asinf as asinf32;
pub use self::atanf as atanf32;
pub use self::atan2f as atan2f32;
pub use self::coshf as coshf32;
pub use self::sinhf as sinhf32;
pub use self::tanf as tanf32;
pub use self::tanhf as tanhf32;

#[inline]
pub unsafe fn asinhf32(n: f32) -> f32 {
    if n == NEG_INFINITY {
        NEG_INFINITY
    } else {
        logf32(n + sqrtf32((n * n) + 1.0))
    }
}
#[inline]
pub unsafe fn acoshf32(n: f32) -> f32 {
    match n {
        x if x < 1.0 => NAN,
        x => logf32(x + sqrtf32((x * x) - 1.0)),
    }
}
#[inline]
pub unsafe fn atanhf32(n: f32) -> f32 {
    0.5 * log1pf((2.0 * n) / (1.0 - n))
}
