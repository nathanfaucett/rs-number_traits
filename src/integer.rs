use core::ops::*;
use core::mem;
use core::num::Wrapping;

use super::Num;

pub trait Integer
    : Num
    + Not<Output = Self>
    + BitAnd<Output = Self>
    + BitOr<Output = Self>
    + BitXor<Output = Self>
    + Shl<usize, Output = Self>
    + Shr<usize, Output = Self> {
    fn count_ones(&self) -> u32;
    fn count_zeros(&self) -> u32;
    fn leading_zeros(&self) -> u32;
    fn trailing_zeros(&self) -> u32;
    fn rotate_left(&self, n: u32) -> Self;
    fn rotate_right(&self, n: u32) -> Self;
    fn signed_shl(&self, n: u32) -> Self;
    fn signed_shr(&self, n: u32) -> Self;
    fn integer_shl(&self, n: u32) -> Self;
    fn integer_shr(&self, n: u32) -> Self;
    fn swap_bytes(&self) -> Self;
    fn from_be(x: &Self) -> Self;
    fn from_le(x: &Self) -> Self;
    fn to_be(&self) -> Self;
    fn to_le(&self) -> Self;
    fn pow(&self, exp: u32) -> Self;
    /// # Examples
    /// ~~~
    /// use number_traits::Integer;
    ///
    /// assert_eq!(Integer::gcd(&6, &8), 2);
    /// assert_eq!(Integer::gcd(&7, &3), 1);
    /// ~~~
    fn gcd(&self, other: &Self) -> Self;
    /// # Examples
    /// ~~~
    /// use number_traits::Integer;
    ///
    /// assert_eq!(Integer::lcm(&7, &3), 21);
    /// assert_eq!(Integer::lcm(&2, &4), 4);
    /// ~~~
    fn lcm(&self, other: &Self) -> Self;
    /// # Examples
    /// ~~~
    /// use number_traits::Integer;
    ///
    /// assert_eq!(Integer::is_multiple_of(&9, &3), true);
    /// assert_eq!(Integer::is_multiple_of(&3, &9), false);
    /// ~~~
    fn is_multiple_of(&self, other: &Self) -> bool;
    /// # Examples
    /// ~~~
    /// use number_traits::Integer;
    ///
    /// assert_eq!(Integer::is_even(&3), false);
    /// assert_eq!(Integer::is_even(&4), true);
    /// ~~~
    fn is_even(&self) -> bool;
    /// # Examples
    /// ~~~
    /// use number_traits::Integer;
    ///
    /// assert_eq!(Integer::is_odd(&3), true);
    /// assert_eq!(Integer::is_odd(&4), false);
    /// ~~~
    fn is_odd(&self) -> bool;
}

macro_rules! impl_core {
    ($T:ty, $S:ty, $U:ty) => {
        #[inline(always)]
        fn count_ones(&self) -> u32 {
            <$T>::count_ones(*self)
        }
        #[inline(always)]
        fn count_zeros(&self) -> u32 {
            <$T>::count_zeros(*self)
        }
        #[inline(always)]
        fn leading_zeros(&self) -> u32 {
            <$T>::leading_zeros(*self)
        }
        #[inline(always)]
        fn trailing_zeros(&self) -> u32 {
            <$T>::trailing_zeros(*self)
        }
        #[inline(always)]
        fn rotate_left(&self, n: u32) -> Self {
            <$T>::rotate_left(*self, n)
        }
        #[inline(always)]
        fn rotate_right(&self, n: u32) -> Self {
            <$T>::rotate_right(*self, n)
        }
        #[inline(always)]
        fn signed_shl(&self, n: u32) -> Self {
            ((*self as $S) << n) as $T
        }
        #[inline(always)]
        fn signed_shr(&self, n: u32) -> Self {
            ((*self as $S) >> n) as $T
        }
        #[inline(always)]
        fn integer_shl(&self, n: u32) -> Self {
            ((*self as $U) << n) as $T
        }
        #[inline(always)]
        fn integer_shr(&self, n: u32) -> Self {
            ((*self as $U) >> n) as $T
        }
        #[inline(always)]
        fn swap_bytes(&self) -> Self {
            <$T>::swap_bytes(*self)
        }
        #[inline(always)]
        fn from_be(x: &Self) -> Self {
            <$T>::from_be(*x)
        }
        #[inline(always)]
        fn from_le(x: &Self) -> Self {
            <$T>::from_le(*x)
        }
        #[inline(always)]
        fn to_be(&self) -> Self {
            <$T>::to_be(*self)
        }
        #[inline(always)]
        fn to_le(&self) -> Self {
            <$T>::to_le(*self)
        }
        #[inline(always)]
        fn pow(&self, exp: u32) -> Self {
            <$T>::pow(*self, exp)
        }
        #[inline]
        fn is_multiple_of(&self, other: &Self) -> bool {
            *self % *other == 0
        }
        #[inline]
        fn is_even(&self) -> bool {
            (*self) & 1 == 0
        }
        #[inline]
        fn is_odd(&self) -> bool {
            !self.is_even()
        }
    }
}

macro_rules! impl_gcd_lcd {
    ($T:ty, unsigned) => (
        #[inline]
        fn gcd(&self, other: &Self) -> Self {
            let mut m = *self;
            let mut n = *other;

            if m == 0 || n == 0 {
                m | n
            } else {
                let shift = (m | n).trailing_zeros();

                if m == Self::min_value() || n == Self::min_value() {
                    1 << shift
                } else {
                    m = m;
                    n = n;
                    n >>= n.trailing_zeros();

                    while m != 0 {
                        m >>= m.trailing_zeros();
                        if n > m {
                            mem::swap(&mut n, &mut m)
                        }
                        m -= n;
                    }

                    n << shift
                }
            }
        }
        #[inline]
        fn lcm(&self, other: &Self) -> Self {
            *self * (*other / self.gcd(other))
        }
    );
    ($T:ty, signed) => (
        #[inline]
        fn gcd(&self, other: &Self) -> Self {
            let mut m = *self;
            let mut n = *other;

            if m == 0 || n == 0 {
                <$T>::abs(m | n)
            } else {
                let shift = (m | n).trailing_zeros();

                if m == Self::min_value() || n == Self::min_value() {
                    <$T>::abs(1 << shift)
                } else {
                    m = <$T>::abs(m);
                    n = <$T>::abs(n);
                    n >>= n.trailing_zeros();

                    while m != 0 {
                        m >>= m.trailing_zeros();
                        if n > m {
                            mem::swap(&mut n, &mut m)
                        }
                        m -= n;
                    }

                    n << shift
                }
            }
        }
        #[inline]
        fn lcm(&self, other: &Self) -> Self {
            <$T>::abs(*self * (*other / self.gcd(other)))
        }
    );
}

macro_rules! impl_integer {
    ($T:ty, $S:ty, $U:ty, signed) => (
        impl Integer for $T {
            impl_core!($T, $S, $U);
            impl_gcd_lcd!($T, signed);
        }
    );
    ($T:ty, $S:ty, $U:ty, unsigned) => (
        impl Integer for $T {
            impl_core!($T, $S, $U);
            impl_gcd_lcd!($T, unsigned);
        }
    );
}

impl_integer!(u8, i8, u8, unsigned);
impl_integer!(u16, i16, u16, unsigned);
impl_integer!(u32, i32, u32, unsigned);
impl_integer!(u64, i64, u64, unsigned);
impl_integer!(u128, i128, u128, unsigned);
impl_integer!(usize, isize, usize, unsigned);

impl_integer!(i8, i8, u8, signed);
impl_integer!(i16, i16, u16, signed);
impl_integer!(i32, i32, u32, signed);
impl_integer!(i64, i64, u64, signed);
impl_integer!(i128, i128, u128, signed);
impl_integer!(isize, isize, usize, signed);

impl<T> Integer for Wrapping<T>
where
    T: Integer,
    Self: Num
        + Not<Output = Self>
        + BitAnd<Output = Self>
        + BitOr<Output = Self>
        + BitXor<Output = Self>
        + Shl<usize, Output = Self>
        + Shr<usize, Output = Self>,
{
    #[inline(always)]
    fn count_ones(&self) -> u32 {
        Integer::count_ones(&self.0)
    }
    #[inline(always)]
    fn count_zeros(&self) -> u32 {
        Integer::count_zeros(&self.0)
    }
    #[inline(always)]
    fn leading_zeros(&self) -> u32 {
        Integer::leading_zeros(&self.0)
    }
    #[inline(always)]
    fn trailing_zeros(&self) -> u32 {
        Integer::trailing_zeros(&self.0)
    }
    #[inline(always)]
    fn rotate_left(&self, n: u32) -> Self {
        Wrapping(Integer::rotate_left(&self.0, n))
    }
    #[inline(always)]
    fn rotate_right(&self, n: u32) -> Self {
        Wrapping(Integer::rotate_right(&self.0, n))
    }
    #[inline(always)]
    fn signed_shl(&self, n: u32) -> Self {
        Wrapping(Integer::signed_shl(&self.0, n))
    }
    #[inline(always)]
    fn signed_shr(&self, n: u32) -> Self {
        Wrapping(Integer::signed_shr(&self.0, n))
    }
    #[inline(always)]
    fn integer_shl(&self, n: u32) -> Self {
        Wrapping(Integer::integer_shl(&self.0, n))
    }
    #[inline(always)]
    fn integer_shr(&self, n: u32) -> Self {
        Wrapping(Integer::integer_shr(&self.0, n))
    }
    #[inline(always)]
    fn swap_bytes(&self) -> Self {
        Wrapping(Integer::swap_bytes(&self.0))
    }
    #[inline(always)]
    fn from_be(x: &Self) -> Self {
        Wrapping(Integer::from_be(&x.0))
    }
    #[inline(always)]
    fn from_le(x: &Self) -> Self {
        Wrapping(Integer::from_le(&x.0))
    }
    #[inline(always)]
    fn to_be(&self) -> Self {
        Wrapping(Integer::to_be(&self.0))
    }
    #[inline(always)]
    fn to_le(&self) -> Self {
        Wrapping(Integer::to_le(&self.0))
    }
    #[inline(always)]
    fn pow(&self, exp: u32) -> Self {
        Wrapping(Integer::pow(&self.0, exp))
    }
    #[inline(always)]
    fn gcd(&self, other: &Self) -> Self {
        Wrapping(Integer::gcd(&self.0, &other.0))
    }
    #[inline(always)]
    fn lcm(&self, other: &Self) -> Self {
        Wrapping(Integer::lcm(&self.0, &other.0))
    }
    #[inline(always)]
    fn is_multiple_of(&self, other: &Self) -> bool {
        Integer::is_multiple_of(&self.0, &other.0)
    }
    #[inline(always)]
    fn is_even(&self) -> bool {
        Integer::is_even(&self.0)
    }
    #[inline(always)]
    fn is_odd(&self) -> bool {
        Integer::is_odd(&self.0)
    }
}
