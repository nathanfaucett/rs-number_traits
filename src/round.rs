use core::intrinsics::{ceilf32, ceilf64, floorf32, floorf64, roundf32, roundf64};
use core::num::Wrapping;

pub trait Round {
    fn floor(&self) -> Self;
    fn ceil(&self) -> Self;
    fn round(&self) -> Self;
}

macro_rules! trait_round {
    ($($T:ident),*) => (
        $(impl Round for $T {
            #[inline(always)]
            fn floor(&self) -> Self { *self }
            #[inline(always)]
            fn ceil(&self) -> Self { *self }
            #[inline(always)]
            fn round(&self) -> Self { *self }
        })*
    );
}

trait_round!(
    usize,
    u8,
    u16,
    u32,
    u64,
    u128,
    isize,
    i8,
    i16,
    i32,
    i64,
    i128
);

impl Round for f32 {
    #[inline(always)]
    fn floor(&self) -> Self {
        unsafe { floorf32(*self) }
    }
    #[inline(always)]
    fn ceil(&self) -> Self {
        unsafe { ceilf32(*self) }
    }
    #[inline(always)]
    fn round(&self) -> Self {
        unsafe { roundf32(*self) }
    }
}
impl Round for f64 {
    #[inline(always)]
    fn floor(&self) -> Self {
        unsafe { floorf64(*self) }
    }
    #[inline(always)]
    fn ceil(&self) -> Self {
        unsafe { ceilf64(*self) }
    }
    #[inline(always)]
    fn round(&self) -> Self {
        unsafe { roundf64(*self) }
    }
}

impl<T> Round for Wrapping<T>
where
    T: Round,
{
    #[inline(always)]
    fn floor(&self) -> Self {
        Wrapping(Round::floor(&self.0))
    }
    #[inline(always)]
    fn ceil(&self) -> Self {
        Wrapping(Round::ceil(&self.0))
    }
    #[inline(always)]
    fn round(&self) -> Self {
        Wrapping(Round::round(&self.0))
    }
}

#[test]
fn test_round() {
    assert_eq!((1.2f32).round(), 1f32);
    assert_eq!((1.8f32).round(), 2f32);
    assert_eq!((-1.2f32).round(), -1f32);
    assert_eq!((-1.8f32).round(), -2f32);

    assert_eq!((1.2f32).floor(), 1f32);
    assert_eq!((1.8f32).floor(), 1f32);
    assert_eq!((-1.2f32).floor(), -2f32);
    assert_eq!((-1.8f32).floor(), -2f32);

    assert_eq!((1.2f32).ceil(), 2f32);
    assert_eq!((1.8f32).ceil(), 2f32);
    assert_eq!((-1.2f32).ceil(), -1f32);
    assert_eq!((-1.8f32).ceil(), -1f32);

    assert_eq!((2).round(), 2);
    assert_eq!((2).floor(), 2);
    assert_eq!((2).ceil(), 2);
}
