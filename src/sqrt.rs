use core::intrinsics::{sqrtf32, sqrtf64};
use core::{f32, f64};
use core::num::Wrapping;

pub trait Sqrt {
    fn sqrt(&self) -> Self;
}

macro_rules! trait_sqrt {
    ($t:ident, $a:ident, $cbrt:ident, $sqrt:ident) => (
        impl Sqrt for $t {
            #[inline(always)]
            fn sqrt(&self) -> Self {
                if *self <= 0 as $t {
                    0 as $t
                } else {
                    unsafe {
                        $sqrt(*self as $a) as $t
                    }
                }
            }
        }
    );
}

macro_rules! trait_sqrt_no_cast {
    ($t:ident, $cbrt:ident, $sqrt:ident) => (
        impl Sqrt for $t {
            #[inline(always)]
            fn sqrt(&self) -> Self {
                if *self < 0.0 {
                    $t::NAN
                } else {
                    unsafe {
                        $sqrt(*self)
                    }
                }
            }
        }
    );
}

trait_sqrt!(usize, f64, cbrt, sqrtf64);
trait_sqrt!(u8, f32, cbrtf, sqrtf32);
trait_sqrt!(u16, f32, cbrtf, sqrtf32);
trait_sqrt!(u32, f64, cbrt, sqrtf64);
trait_sqrt!(u64, f64, cbrt, sqrtf64);
trait_sqrt!(u128, f64, cbrt, sqrtf64);

trait_sqrt!(isize, f64, cbrt, sqrtf64);
trait_sqrt!(i8, f32, cbrtf, sqrtf32);
trait_sqrt!(i16, f32, cbrtf, sqrtf32);
trait_sqrt!(i32, f64, cbrt, sqrtf64);
trait_sqrt!(i64, f64, cbrt, sqrtf64);
trait_sqrt!(i128, f64, cbrt, sqrtf64);

trait_sqrt_no_cast!(f32, cbrtf, sqrtf32);
trait_sqrt_no_cast!(f64, cbrt, sqrtf64);

impl<T> Sqrt for Wrapping<T>
where
    T: Sqrt,
{
    #[inline(always)]
    fn sqrt(&self) -> Self {
        Wrapping(Sqrt::sqrt(&self.0))
    }
}

#[cfg(test)]
mod test {
    use super::Sqrt;

    fn sqrt<T: Sqrt>(x: T) -> T {
        x.sqrt()
    }

    #[test]
    fn test_sqrt() {
        assert_eq!(sqrt(4), 2);
        assert_eq!(sqrt(4.0), 2.0);
    }
}
