use core::intrinsics;

use libc::{c_double, c_float};

use super::{One, Zero};

#[link_name = "m"]
extern "C" {
    pub fn expm1f(n: c_float) -> c_float;
    pub fn log1pf(n: c_float) -> c_float;

    pub fn expm1(n: c_double) -> c_double;
    pub fn log1p(n: c_double) -> c_double;
}

pub trait Pow {
    /// ```
    /// use number_traits::{ApproxEq, Pow};
    /// assert_eq!(Pow::exp(&2), 7);
    /// assert!(Pow::exp(&2.0).approx_eq(&7.38905609893065));
    /// ```
    fn exp(&self) -> Self;
    /// ```
    /// use number_traits::{ApproxEq, Pow};
    /// assert_eq!(Pow::exp2(&2), 4);
    /// assert!(Pow::exp2(&2.0).approx_eq(&4.0));
    /// ```
    fn exp2(&self) -> Self;
    /// ```
    /// use number_traits::{ApproxEq, Pow};
    /// assert_eq!(Pow::exp_m1(&2), 6);
    /// assert!(Pow::exp_m1(&2.0).approx_eq(&6.38905609893065));
    /// ```
    fn exp_m1(&self) -> Self;

    /// ```
    /// use number_traits::{ApproxEq, Pow};
    /// assert_eq!(Pow::ln(&2), 0);
    /// assert!(Pow::ln(&2.0).approx_eq(&0.6931471805599453));
    /// ```
    fn ln(&self) -> Self;
    /// ```
    /// use number_traits::{ApproxEq, Pow};
    /// assert_eq!(Pow::ln_1p(&2), 1);
    /// assert!(Pow::ln_1p(&2.0).approx_eq(&1.0986122886681096));
    /// ```
    fn ln_1p(&self) -> Self;

    /// ```
    /// use number_traits::{ApproxEq, Pow};
    /// assert_eq!(Pow::log(&2, &10), 0);
    /// assert!(Pow::log(&2.0, &10.0).approx_eq(&0.30102999566398114));
    /// ```
    fn log(&self, base: &Self) -> Self;
    /// ```
    /// use number_traits::{ApproxEq, Pow};
    /// assert_eq!(Pow::log2(&2), 1);
    /// assert!(Pow::log2(&2.0).approx_eq(&1.0));
    /// ```
    fn log2(&self) -> Self;
    /// ```
    /// use number_traits::{ApproxEq, Pow};
    /// assert_eq!(Pow::log10(&2), 0);
    /// assert!(Pow::log10(&2.0).approx_eq(&0.30102999566398114));
    /// ```
    fn log10(&self) -> Self;

    /// ```
    /// use number_traits::Pow;
    /// assert_eq!(Pow::pow(&2, &2), 4);
    /// assert_eq!(Pow::pow(&2.0, &2.0), 4.0);
    /// ```
    fn pow(&self, exp: &Self) -> Self;
}

impl Pow for f32 {
    #[cfg(target_env = "msvc")]
    #[inline(always)]
    fn exp(&self) -> Self {
        (*self as f64).exp() as f32
    }
    #[cfg(not(target_env = "msvc"))]
    #[inline(always)]
    fn exp(&self) -> Self {
        unsafe { intrinsics::expf32(*self) }
    }
    #[inline(always)]
    fn exp2(&self) -> Self {
        unsafe { intrinsics::exp2f32(*self) }
    }
    #[inline]
    fn exp_m1(&self) -> Self {
        unsafe { expm1f(*self) }
    }

    #[cfg(target_env = "msvc")]
    #[inline(always)]
    fn ln(&self) -> Self {
        (*self as f64).ln() as f32
    }
    #[cfg(not(target_env = "msvc"))]
    #[inline(always)]
    fn ln(&self) -> Self {
        unsafe { intrinsics::logf32(*self) }
    }
    fn ln_1p(&self) -> Self {
        unsafe { log1pf(*self) }
    }

    #[inline]
    fn log(&self, base: &Self) -> Self {
        self.ln() / base.ln()
    }
    #[cfg(target_os = "android")]
    #[inline(always)]
    fn log2(&self) -> Self {
        self.ln() * ::core::f32::consts::LOG2_E
    }
    #[cfg(not(target_os = "android"))]
    #[inline(always)]
    fn log2(&self) -> Self {
        unsafe { intrinsics::log2f32(*self) }
    }
    #[cfg(target_env = "msvc")]
    #[inline(always)]
    fn log10(&self) -> Self {
        (*self as f64).log10() as f32
    }
    #[cfg(not(target_env = "msvc"))]
    #[inline(always)]
    fn log10(&self) -> Self {
        unsafe { intrinsics::log10f32(*self) }
    }

    #[inline]
    fn pow(&self, exp: &Self) -> Self {
        unsafe { intrinsics::powf32(*self, *exp) }
    }
}

impl Pow for f64 {
    #[inline(always)]
    fn exp(&self) -> Self {
        unsafe { intrinsics::expf64(*self) }
    }
    #[inline(always)]
    fn exp2(&self) -> Self {
        unsafe { intrinsics::exp2f64(*self) }
    }
    #[inline]
    fn exp_m1(&self) -> Self {
        unsafe { expm1(*self) }
    }

    #[inline]
    fn ln(&self) -> Self {
        unsafe { intrinsics::logf64(*self) }
    }
    fn ln_1p(&self) -> Self {
        unsafe { log1p(*self) }
    }

    #[inline]
    fn log(&self, base: &Self) -> Self {
        self.ln() / base.ln()
    }
    #[inline(always)]
    fn log2(&self) -> Self {
        unsafe { intrinsics::log2f64(*self) }
    }
    #[inline(always)]
    fn log10(&self) -> Self {
        unsafe { intrinsics::log10f64(*self) }
    }

    #[inline]
    fn pow(&self, exp: &Self) -> Self {
        unsafe { intrinsics::powf64(*self, *exp) }
    }
}

macro_rules! trait_pow_integer {
    ($F:ty, $($T:ty),*) => (
        $(impl Pow for $T {
            #[inline(always)]
            fn exp(&self) -> Self {
                Pow::exp(&(*self as $F)) as $T
            }
            #[inline(always)]
            fn exp2(&self) -> Self {
                Pow::exp2(&(*self as $F)) as $T
            }
            #[inline]
            fn exp_m1(&self) -> Self {
                Pow::exp_m1(&(*self as $F)) as $T
            }

            #[inline]
            fn ln(&self) -> Self {
                Pow::ln(&(*self as $F)) as $T
            }
            fn ln_1p(&self) -> Self {
                Pow::ln_1p(&(*self as $F)) as $T
            }

            #[inline(always)]
            fn log(&self, base: &Self) -> Self {
                Pow::log(&(*self as $F), &(*base as $F)) as $T
            }
            #[inline(always)]
            fn log2(&self) -> Self {
                Pow::log2(&(*self as $F)) as $T
            }
            #[inline(always)]
            fn log10(&self) -> Self {
                Pow::log10(&(*self as $F)) as $T
            }

            #[inline]
            fn pow(&self, exp: &Self) -> Self {
                let mut base = *self;
                let mut exp = *exp;

                if exp == 0 {
                    One::one()
                } else if exp < 1 {
                    Zero::zero()
                } else {
                    while exp & 1 == 0 {
                        base = base.clone() * base;
                        exp >>= 1;
                    }

                    if exp == 1 {
                        base
                    } else {
                        let mut acc = base.clone();

                        while exp > 1 {
                            exp >>= 1;
                            base = base.clone() * base;

                            if exp & 1 == 1 {
                                acc = acc * base.clone();
                            }
                        }

                        acc
                    }
                }
            }
        })*
    );
}

trait_pow_integer!(f32, u8, u16, i8, i16);
trait_pow_integer!(f64, u32, u64, u128, i32, i64, i128, usize, isize);
