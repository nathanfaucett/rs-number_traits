use core::num::{FpCategory, Wrapping};
use core::{intrinsics, mem, f32, f64};

use libc::{c_double, c_float};

use super::Signed;

#[link_name = "m"]
extern "C" {
    pub fn hypot(x: c_double, y: c_double) -> c_double;
    pub fn cbrtf(n: c_float) -> c_float;
    pub fn cbrt(n: c_double) -> c_double;
}

pub trait Float: Signed {
    fn nan() -> Self;
    fn infinity() -> Self;
    fn neg_infinity() -> Self;
    fn neg_zero() -> Self;
    fn epsilon() -> Self;
    fn is_nan(&self) -> bool;
    fn is_infinite(&self) -> bool;
    fn is_finite(&self) -> bool;
    fn is_normal(&self) -> bool;
    fn classify(&self) -> FpCategory;
    fn trunc(&self) -> Self;
    fn fract(&self) -> Self;
    fn is_sign_positive(&self) -> bool;
    fn is_sign_negative(&self) -> bool;
    fn recip(&self) -> Self;
    fn powi(&self, n: i32) -> Self;
    fn powf(&self, n: &Self) -> Self;
    fn cbrt(&self) -> Self;
    fn hypot(&self, other: &Self) -> Self;
    fn integer_decode(&self) -> (u64, i16, i8);
}

macro_rules! impl_core_float {
    ($T:ident) => (
        #[inline(always)]
        fn nan() -> Self {
            ::core::$T::NAN
        }
        #[inline(always)]
        fn infinity() -> Self {
            ::core::$T::INFINITY
        }
        #[inline(always)]
        fn neg_infinity() -> Self {
            ::core::$T::NEG_INFINITY
        }
        #[inline(always)]
        fn neg_zero() -> Self {
            -0.0
        }
        #[inline(always)]
        fn epsilon() -> Self {
            ::core::$T::EPSILON
        }
        #[inline(always)]
        fn is_nan(&self) -> bool {
             *self != *self
        }
        #[inline(always)]
        fn is_infinite(&self) -> bool {
             *self == ::core::$T::INFINITY || *self == ::core::$T::NEG_INFINITY
        }
        #[inline(always)]
        fn is_finite(&self) -> bool {
            !(self.is_nan() || self.is_infinite())
        }
        #[inline(always)]
        fn is_normal(&self) -> bool {
            self.classify() == FpCategory::Normal
        }
        #[inline(always)]
        fn fract(&self) -> Self {
            *self - self.trunc()
        }
        #[inline(always)]
        fn is_sign_positive(&self) -> bool {
            *self > 0.0 || (1.0 / *self) == ::core::$T::INFINITY
        }
        #[inline(always)]
        fn is_sign_negative(&self) -> bool {
            *self < 0.0 || (1.0 / *self) == ::core::$T::NEG_INFINITY
        }
        #[inline(always)]
        fn recip(&self) -> Self {
            1.0 / *self
        }
    )
}

impl Float for f32 {
    impl_core_float!(f32);

    #[inline]
    fn classify(&self) -> FpCategory {
        const EXP_MASK: u32 = 0x7f800000;
        const MAN_MASK: u32 = 0x007fffff;

        let bits: u32 = unsafe { mem::transmute(*self) };
        match (bits & MAN_MASK, bits & EXP_MASK) {
            (0, 0) => FpCategory::Zero,
            (_, 0) => FpCategory::Subnormal,
            (0, EXP_MASK) => FpCategory::Infinite,
            (_, EXP_MASK) => FpCategory::Nan,
            _ => FpCategory::Normal,
        }
    }
    #[inline(always)]
    fn trunc(&self) -> Self {
        unsafe { intrinsics::truncf32(*self) }
    }

    #[inline(always)]
    fn powi(&self, n: i32) -> Self {
        unsafe { intrinsics::powif32(*self, n) }
    }
    #[inline(always)]
    fn powf(&self, n: &Self) -> Self {
        unsafe { intrinsics::powf32(*self, *n) }
    }

    #[inline(always)]
    fn cbrt(&self) -> Self {
        if *self < 0.0 {
            f32::NAN
        } else {
            unsafe { cbrtf(*self) }
        }
    }

    /// ```
    /// use number_traits::Float;
    /// assert_eq!(Float::hypot(&1.0_f32, &1.0_f32), 1.4142135_f32);
    /// ```
    #[inline(always)]
    fn hypot(&self, other: &Self) -> Self {
        unsafe { hypot(*self as f64, *other as f64) as f32 }
    }
    /// ```
    /// use number_traits::Float;
    /// let num = 2.0f32;
    ///
    /// // (8388608, -22, 1)
    /// let (mantissa, exponent, sign) = Float::integer_decode(&num);
    /// let sign_f = sign as f32;
    /// let mantissa_f = mantissa as f32;
    /// let exponent_f = num.powf(exponent as f32);
    ///
    /// // 1 * 8388608 * 2^(-22) == 2
    /// let abs_difference = (sign_f * mantissa_f * exponent_f - num).abs();
    /// assert!(abs_difference < 1e-10);
    /// ```
    #[inline]
    fn integer_decode(&self) -> (u64, i16, i8) {
        let bits: u32 = unsafe { mem::transmute(*self) };
        let sign: i8 = if bits >> 31 == 0 { 1 } else { -1 };
        let mut exponent: i16 = ((bits >> 23) & 0xff) as i16;
        let mantissa = if exponent == 0 {
            (bits & 0x7fffff) << 1
        } else {
            (bits & 0x7fffff) | 0x800000
        };
        exponent -= 127 + 23;
        (mantissa as u64, exponent, sign)
    }
}

impl Float for f64 {
    impl_core_float!(f64);

    #[inline]
    fn classify(&self) -> FpCategory {
        const EXP_MASK: u64 = 0x7ff0000000000000;
        const MAN_MASK: u64 = 0x000fffffffffffff;

        let bits: u64 = unsafe { mem::transmute(*self) };
        match (bits & MAN_MASK, bits & EXP_MASK) {
            (0, 0) => FpCategory::Zero,
            (_, 0) => FpCategory::Subnormal,
            (0, EXP_MASK) => FpCategory::Infinite,
            (_, EXP_MASK) => FpCategory::Nan,
            _ => FpCategory::Normal,
        }
    }
    #[inline(always)]
    fn trunc(&self) -> Self {
        unsafe { intrinsics::truncf64(*self) }
    }
    #[inline(always)]
    fn powi(&self, n: i32) -> Self {
        unsafe { intrinsics::powif64(*self as f64, n) }
    }

    #[inline(always)]
    fn powf(&self, n: &Self) -> Self {
        unsafe { intrinsics::powf64(*self, *n) }
    }

    #[inline(always)]
    fn cbrt(&self) -> Self {
        if *self < 0.0 {
            f64::NAN
        } else {
            unsafe { cbrt(*self) }
        }
    }

    /// ```
    /// use number_traits::Float;
    /// assert_eq!(Float::hypot(&1.0_f64, &1.0_f64), 1.4142135623730951_f64);
    /// ```
    #[inline(always)]
    fn hypot(&self, other: &Self) -> Self {
        unsafe { hypot(*self, *other) }
    }
    #[inline]
    fn integer_decode(&self) -> (u64, i16, i8) {
        let bits: u64 = unsafe { mem::transmute(*self) };
        let sign: i8 = if bits >> 63 == 0 { 1 } else { -1 };
        let mut exponent: i16 = ((bits >> 52) & 0x7ff) as i16;
        let mantissa = if exponent == 0 {
            (bits & 0xfffffffffffff) << 1
        } else {
            (bits & 0xfffffffffffff) | 0x10000000000000
        };
        exponent -= 1023 + 52;
        (mantissa, exponent, sign)
    }
}

impl<T> Float for Wrapping<T>
where
    T: Float,
    Self: Signed,
{
    #[inline(always)]
    fn nan() -> Self {
        Wrapping(Float::nan())
    }
    #[inline(always)]
    fn infinity() -> Self {
        Wrapping(Float::infinity())
    }
    #[inline(always)]
    fn neg_infinity() -> Self {
        Wrapping(Float::neg_infinity())
    }
    #[inline(always)]
    fn neg_zero() -> Self {
        Wrapping(Float::neg_zero())
    }
    #[inline(always)]
    fn epsilon() -> Self {
        Wrapping(Float::epsilon())
    }
    #[inline(always)]
    fn is_nan(&self) -> bool {
        Float::is_nan(&self.0)
    }
    #[inline(always)]
    fn is_infinite(&self) -> bool {
        Float::is_infinite(&self.0)
    }
    #[inline(always)]
    fn is_finite(&self) -> bool {
        Float::is_finite(&self.0)
    }
    #[inline(always)]
    fn is_normal(&self) -> bool {
        Float::is_normal(&self.0)
    }
    #[inline(always)]
    fn classify(&self) -> FpCategory {
        Float::classify(&self.0)
    }
    #[inline(always)]
    fn trunc(&self) -> Self {
        Wrapping(Float::trunc(&self.0))
    }
    #[inline(always)]
    fn fract(&self) -> Self {
        Wrapping(Float::fract(&self.0))
    }
    #[inline(always)]
    fn is_sign_positive(&self) -> bool {
        Float::is_sign_positive(&self.0)
    }
    #[inline(always)]
    fn is_sign_negative(&self) -> bool {
        Float::is_sign_negative(&self.0)
    }
    #[inline(always)]
    fn recip(&self) -> Self {
        Wrapping(Float::recip(&self.0))
    }
    #[inline(always)]
    fn powi(&self, n: i32) -> Self {
        Wrapping(Float::powi(&self.0, n))
    }
    #[inline(always)]
    fn powf(&self, n: &Self) -> Self {
        Wrapping(Float::powf(&self.0, &n.0))
    }
    #[inline(always)]
    fn cbrt(&self) -> Self {
        Wrapping(Float::cbrt(&self.0))
    }
    #[inline(always)]
    fn hypot(&self, other: &Self) -> Self {
        Wrapping(Float::hypot(&self.0, &other.0))
    }
    #[inline(always)]
    fn integer_decode(&self) -> (u64, i16, i8) {
        Float::integer_decode(&self.0)
    }
}
